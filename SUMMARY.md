# Contents

## Meta

* [About this project](README.md)

* [Changes](ALTERATIONS.md)

## Structure and Interpretation of Computer Programs

* [Foreword](book/foreword.md)

* [Preface to the Second Edition](book/preface-to-the-second-edition.md)

* [Preface to the First Edition](book/preface-to-the-first-edition.md)

* [Acknowledgments](book/acknowledgments.md)

* [1 Building Abstractions with Procedures](book/ch1/1-building-abstractions-with-procedures.md)
  * [1.1 The Elements of Programming](book/ch1/1-1-the-elements-of-programming.md)
    * [1.1.1 Expressions](book/ch1/1-1-1-expressions.md)
    * [1.1.2 Naming and the Environment](book/ch1/1-1-2-naming-and-the-environment.md)
    * [1.1.3 Evaluating Combinations](book/ch1/1-1-3-evaluating-combinations.md)
    * [1.1.4 Compound Procedures](book/ch1/1-1-4-compound-procedures.md)
    * [1.1.5 The Substitution Model for Procedure Application](book/ch1/1-1-5-the-substitution-model-for-procedure-application.md)
    * [1.1.6 Conditional Expressions and Predicates](book/ch1/1-1-6-conditional-expressions-and-predicates.md)
    * [1.1.7 Example: Square Roots by Newton's Method](book/ch1/1-1-7-example-square-roots-by-newtons-method.md)
    * [1.1.8 Procedures as Black-Box Abstractions](book/ch1/1-1-8-procedures-as-black-box-abstractions.md)
  * [1.2 Procedures and the Processes They Generate](book/ch1/1-2-procedures-and-the-processes-they-generate.md)
    * [1.2.1 Linear Recursion and Iteration](book/ch1/1-2-1-linear-recursion-and-iteration.md)
    * [1.2.2 Tree Recursion](book/ch1/1-2-2-tree-recursion.md)
