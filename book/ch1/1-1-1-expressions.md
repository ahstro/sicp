# 1.1.1  Expressions

One easy way to get started at programming is to examine some typical interactions with an interpreter for the Scheme dialect of Lisp. Imagine that you are sitting at a computer terminal. You type an _expression_, and the interpreter responds by displaying the result of its _evaluating_ that expression.

One kind of primitive expression you might type is a number. (More precisely, the expression that you type consists of the numerals that represent the number in base 10.) If you present Lisp with a number

<pre>
486
</pre>

the interpreter will respond by printing[^5]

<pre>
<em>486</em>
</pre>

Expressions representing numbers may be combined with an expression representing a primitive procedure (such as `+` or `*`) to form a compound expression that represents the application of the procedure to those numbers. For example:

<pre>
(+ 137 349)
<em>486</em>
(- 1000 334)
<em>666</em>
(* 5 99)
<em>495</em>
(/ 10 5)
<em>2</em>
(+ 2.7 10)
<em>12.7</em>
</pre>

Expressions such as these, formed by delimiting a list of expressions within parentheses in order to denote procedure application, are called _combinations_. The leftmost element in the list is called the _operator_, and the other elements are called _operands_. The value of a combination is obtained by applying the procedure specified by the operator to the _arguments_ that are the values of the operands.

The convention of placing the operator to the left of the operands is known as _prefix notation_, and it may be somewhat confusing at first because it departs significantly from the customary mathematical convention. Prefix notation has several advantages, however. One of them is that it can accommodate procedures that may take an arbitrary number of arguments, as in the following examples:

<pre>
(+ 21 35 12 7)
<em>75</em>
</pre>

<pre>
(* 25 4 12)
<em>1200</em>
</pre>

No ambiguity can arise, because the operator is always the leftmost element and the entire combination is delimited by the parentheses.

A second advantage of prefix notation is that it extends in a straightforward way to allow combinations to be _nested_, that is, to have combinations whose elements are themselves combinations:

<pre>
(+ (* 3 5) (- 10 6))
<em>19</em>
</pre>

There is no limit (in principle) to the depth of such nesting and to the overall complexity of the expressions that the Lisp interpreter can evaluate. It is we humans who get confused by still relatively simple expressions such as

<pre>
(+ (* 3 (+ (* 2 4) (+ 3 5))) (+ (- 10 7) 6))
</pre>

which the interpreter would readily evaluate to be 57. We can help ourselves by writing such an expression in the form

<pre>
(+ (* 3
      (+ (* 2 4)
         (+ 3 5)))
   (+ (- 10 7)
      6))
</pre>

following a formatting convention known as _pretty-printing_, in which each long combination is written so that the operands are aligned vertically. The resulting indentations display clearly the structure of the expression.[^6]

Even with complex expressions, the interpreter always operates in the same basic cycle: It reads an expression from the terminal, evaluates the expression, and prints the result. This mode of operation is often expressed by saying that the interpreter runs in a _read-eval-print loop_. Observe in particular that it is not necessary to explicitly instruct the interpreter to print the value of the expression.[^7]

---

[^5]: Throughout this book, when we wish to emphasize the distinction between the input typed by the user and the response printed by the interpreter, we will show the latter in slanted characters.

[^6]: Lisp systems typically provide features to aid the user in formatting expressions. Two especially useful features are one that automatically indents to the proper pretty-print position whenever a new line is started and one that highlights the matching left parenthesis whenever a right parenthesis is typed.

[^7]: Lisp obeys the convention that every expression has a value. This convention, together with the old reputation of Lisp as an inefficient language, is the source of the quip by Alan Perlis (paraphrasing Oscar Wilde) that "Lisp programmers know the value of everything but the cost of nothing."
