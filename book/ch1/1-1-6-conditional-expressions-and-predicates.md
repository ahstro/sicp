# 1.1.6  Conditional Expressions and Predicates

The expressive power of the class of procedures that we can define at this point is very limited, because we have no way to make tests and to perform different operations depending on the result of a test. For instance, we cannot define a procedure that computes the absolute value of a number by testing whether the number is positive, negative, or zero and taking different actions in the different cases according to the rule

![](images/1.gif)

This construct is called a _case analysis_, and there is a special form in Lisp for notating such a case analysis. It is called `cond` (which stands for "conditional"), and it is used as follows:

<pre>
(define (abs x)
  (cond ((> x 0) x)
        ((= x 0) 0)
        ((< x 0) (- x))))
</pre>

The general form of a conditional expression is

<pre>
(cond (<<em>p</em><sub>1</sub>> <<em>e</em><sub>1</sub>>)
      (<<em>p</em><sub>2</sub>> <<em>e</em><sub>2</sub>>)
      ⋮
      (<<em>p</em><sub>n</sub>> <<em>e</em><sub>n</sub>>))
</pre>

consisting of the symbol `cond` followed by parenthesized pairs of expressions `(<p> <e>)` called clauses. The first expression in each pair is a _predicate_ -- that is, an expression whose value is interpreted as either true or false.[^17]

Conditional expressions are evaluated as follows. The predicate <<em>p</em><sub>1</sub>> is evaluated first. If its value is false, then <<em>p</em><sub>2</sub>> is evaluated. If <<em>p</em><sub>2</sub>>'s value is also false, then <<em>p</em><sub>3</sub>> is evaluated. This process continues until a predicate is found whose value is true, in which case the interpreter returns the value of the corresponding _consequent expression_ <<em>e</em>> of the clause as the value of the conditional expression. If none of the <<em>p</em>>'s is found to be true, the value of the `cond` is undefined.

The word _predicate_ is used for procedures that return true or false, as well as for expressions that evaluate to true or false. The absolute-value procedure `abs` makes use of the primitive predicates `>`, `<`, and `=`.[^18] These take two numbers as arguments and test whether the first number is, respectively, greater than, less than, or equal to the second number, returning true or false accordingly.

Another way to write the absolute-value procedure is

<pre>
(define (abs x)
  (cond ((< x 0) (- x))
        (else x)))
</pre>

which could be expressed in English as "If _x_ is less than zero return - _x_; otherwise return _x_." `Else` is a special symbol that can be used in place of the <<em>p</em>> in the final clause of a `cond`. This causes the `cond` to return as its value the value of the corresponding <<em>e</em>> whenever all previous clauses have been bypassed. In fact, any expression that always evaluates to a true value could be used as the <<em>p</em>> here.

Here is yet another way to write the absolute-value procedure:

<pre>
(define (abs x)
  (if (< x 0)
      (- x)
      x))
</pre>

This uses the special form `if`, a restricted type of conditional that can be used when there are precisely two cases in the case analysis. The general form of an `if` expression is

<pre>
(if <<em>predicate</em>> <<em>consequent</em>> <<em>alternative</em>>)
</pre>

To evaluate an `if` expression, the interpreter starts by evaluating the <<em>predicate</em>> part of the expression. If the <<em>predicate</em>> evaluates to a true value, the interpreter then evaluates the <<em>consequent</em>> and returns its value. Otherwise it evaluates the <<em>alternative</em>> and returns its value.[^19]

In addition to primitive predicates such as `<`, `=`, and `>`, there are logical composition operations, which enable us to construct compound predicates. The three most frequently used are these:


- `(and <e1> ... <en>)`

    The interpreter evaluates the expressions <<em>e</em>> one at a time, in left-to-right order. If any <<em>e</em>> evaluates to false, the value of the `and` expression is false, and the rest of the <<em>e</em>>'s are not evaluated. If all <<em>e</em>>'s evaluate to true values, the value of the `and` expression is the value of the last one.

- `(or <e1> ... <en>)`

    The interpreter evaluates the expressions <<em>e</em>> one at a time, in left-to-right order. If any <<em>e</em>> evaluates to a true value, that value is returned as the value of the `or` expression, and the rest of the <<em>e</em>>'s are not evaluated. If all <<em>e</em>>'s evaluate to false, the value of the `or` expression is false.

- `(not <e>)`

    The value of a `not` expression is true when the expression <<em>e</em>> evaluates to false, and false otherwise. 

Notice that `and` and `or` are special forms, not procedures, because the subexpressions are not necessarily all evaluated. `Not` is an ordinary procedure.

As an example of how these are used, the condition that a number _x_ be in the range 5 < _x_ < 10 may be expressed as

<pre>
(and (> x 5) (< x 10))
</pre>

As another example, we can define a predicate to test whether one number is greater than or equal to another as

<pre>
(define (>= x y)
  (or (> x y) (= x y)))
</pre>

or alternatively as

<pre>
(define (>= x y)
  (not (< x y)))
</pre>

<strong id="exercise1_1">Exercise 1.1</strong>.  Below is a sequence of expressions. What is the result printed by the interpreter in response to each expression? Assume that the sequence is to be evaluated in the order in which it is presented.

<pre>
10
(+ 5 3 4)
(- 9 1)
(/ 6 2)
(+ (* 2 4) (- 4 6))
(define a 3)
(define b (+ a 1))
(+ a b (* a b))
(= a b)
(if (and (> b a) (< b (* a b)))
    b
    a)
(cond ((= a 4) 6)
      ((= b 4) (+ 6 7 a))
      (else 25))
(+ 2 (if (> b a) b a))
(* (cond ((> a b) a)
         ((< a b) b)
         (else -1))
   (+ a 1))
</pre>

<strong id="exercise1_2">Exercise 1.2</strong>.  Translate the following expression into prefix form

![](images/2.gif)

<strong id="exercise1_3">Exercise 1.3</strong>.  Define a procedure that takes three numbers as arguments and returns the sum of the squares of the two larger numbers.

<strong id="exercise1_4">Exercise 1.4</strong>.  Observe that our model of evaluation allows for combinations whose operators are compound expressions. Use this observation to describe the behavior of the following procedure:

<pre>
(define (a-plus-abs-b a b)
  ((if (> b 0) + -) a b))
</pre>

<strong id="exercise1_5">Exercise 1.5</strong>.  Ben Bitdiddle has invented a test to determine whether the interpreter he is faced with is using applicative-order evaluation or normal-order evaluation. He defines the following two procedures:

<pre>
(define (p) (p))

(define (test x y)
  (if (= x 0)
      0
      y))
</pre>

Then he evaluates the expression

<pre>
(test 0 (p))
</pre>

What behavior will Ben observe with an interpreter that uses applicative-order evaluation? What behavior will he observe with an interpreter that uses normal-order evaluation? Explain your answer. (Assume that the evaluation rule for the special form `if` is the same whether the interpreter is using normal or applicative order: The predicate expression is evaluated first, and the result determines whether to evaluate the consequent or the alternative expression.)

---

[^17]: "Interpreted as either true or false" means this: In Scheme, there are two distinguished values that are denoted by the constants `#t` and `#f`. When the interpreter checks a predicate's value, it interprets `#f` as false. Any other value is treated as true. (Thus, providing `#t` is logically unnecessary, but it is convenient.) In this book we will use names `true` and `false`, which are associated with the values `#t` and `#f` respectively.

[^18]: Abs also uses the "minus" operator `-`, which, when used with a single operand, as in `(- x)`, indicates negation.

[^19]: A minor difference between `if` and `cond` is that the <<em>e</em>> part of each `cond` clause may be a sequence of expressions. If the corresponding <<em>p</em>> is found to be true, the expressions <<em>e</em>> are evaluated in sequence and the value of the final expression in the sequence is returned as the value of the `cond`. In an if expression, however, the <<em>consequent</em>> and <<em>alternative</em>> must be single expressions.
