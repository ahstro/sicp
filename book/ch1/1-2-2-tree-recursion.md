# 1.2.2  Tree Recursion

Another common pattern of computation is called _tree recursion_. As an example, consider computing the sequence of Fibonacci numbers, in which each number is the sum of the preceding two:

![](images/7.gif)

In general, the Fibonacci numbers can be defined by the rule

![](images/8.gif)

We can immediately translate this definition into a recursive procedure for computing Fibonacci numbers:

<pre>
(define (fib n)  
  (cond ((= n 0) 0)  
        ((= n 1) 1)  
        (else (+ (fib (- n 1))  
                 (fib (- n 2))))))  
</pre>

# ![](fig/1_5.gif) {#fig1_5}

**Figure 1.5:**  The tree-recursive process generated in computing `(fib 5)`.

Consider the pattern of this computation. To compute `(fib 5)`, we compute `(fib 4)` and `(fib 3)`. To compute `(fib 4)`, we compute `(fib 3)` and `(fib 2)`. In general, the evolved process looks like a tree, as shown in figure [1.5](#fig1_5). Notice that the branches split into two at each level (except at the bottom); this reflects the fact that the `fib` procedure calls itself twice each time it is invoked.

This procedure is instructive as a prototypical tree recursion, but it is a terrible way to compute Fibonacci numbers because it does so much redundant computation. Notice in figure [1.5](#fig1_5) that the entire computation of `(fib 3)` -- almost half the work -- is duplicated. In fact, it is not hard to show that the number of times the procedure will compute `(fib 1)` or `(fib 0)` (the number of leaves in the above tree, in general) is precisely _Fib_(_n_ + 1). To get an idea of how bad this is, one can show that the value of _Fib_(_n_) grows exponentially with _n_. More precisely (see exercise [1.13](#exercise1_13)), _Fib_(_n_) is the closest integer to φ<sup>_n_</sup> /√5, where

![](images/9.gif)

is the _golden ratio_, which satisfies the equation

![](images/10.gif)

Thus, the process uses a number of steps that grows exponentially with the input. On the other hand, the space required grows only linearly with the input, because we need keep track only of which nodes are above us in the tree at any point in the computation. In general, the number of steps required by a tree-recursive process will be proportional to the number of nodes in the tree, while the space required will be proportional to the maximum depth of the tree.

We can also formulate an iterative process for computing the Fibonacci numbers. The idea is to use a pair of integers _a_ and _b_, initialized to _Fib_(1) = 1 and _Fib_(0) = 0, and to repeatedly apply the simultaneous transformations

![](images/11.gif)

It is not hard to show that, after applying this transformation _n_ times, _a_ and _b_ will be equal, respectively, to _Fib_(_n_ + 1) and _Fib_(_n_). Thus, we can compute Fibonacci numbers iteratively using the procedure

<pre>
(define (fib n)  
  (fib-iter 1 0 n))  

(define (fib-iter a b count)  
  (if (= count 0)  
      b  
      (fib-iter (+ a b) a (- count 1))))  
</pre>

This second method for computing _Fib_(_n_) is a linear iteration. The difference in number of steps required by the two methods -- one linear in _n_, one growing as fast as _Fib_(_n_) itself -- is enormous, even for small inputs.

One should not conclude from this that tree-recursive processes are useless. When we consider processes that operate on hierarchically structured data rather than numbers, we will find that tree recursion is a natural and powerful tool.[^32] But even in numerical operations, tree-recursive processes can be useful in helping us to understand and design programs. For instance, although the first `fib` procedure is much less efficient than the second one, it is more straightforward, being little more than a translation into Lisp of the definition of the Fibonacci sequence. To formulate the iterative algorithm required noticing that the computation could be recast as an iteration with three state variables.



#### Example: Counting change

It takes only a bit of cleverness to come up with the iterative Fibonacci algorithm. In contrast, consider the following problem: How many different ways can we make change of $ 1.00, given half-dollars, quarters, dimes, nickels, and pennies? More generally, can we write a procedure to compute the number of ways to change any given amount of money?

This problem has a simple solution as a recursive procedure. Suppose we think of the types of coins available as arranged in some order. Then the following relation holds:

The number of ways to change amount _a_ using _n_ kinds of coins equals

- the number of ways to change amount _a_ using all but the first kind of coin, plus
- the number of ways to change amount _a_ - _d_ using all _n_ kinds of coins, where _d_ is the denomination of the first kind of coin.

To see why this is true, observe that the ways to make change can be divided into two groups: those that do not use any of the first kind of coin, and those that do. Therefore, the total number of ways to make change for some amount is equal to the number of ways to make change for the amount without using any of the first kind of coin, plus the number of ways to make change assuming that we do use the first kind of coin. But the latter number is equal to the number of ways to make change for the amount that remains after using a coin of the first kind.

Thus, we can recursively reduce the problem of changing a given amount to the problem of changing smaller amounts using fewer kinds of coins. Consider this reduction rule carefully, and convince yourself that we can use it to describe an algorithm if we specify the following degenerate cases:[^33]

- If _a_ is exactly 0, we should count that as 1 way to make change.
- If _a_ is less than 0, we should count that as 0 ways to make change.
- If _n_ is 0, we should count that as 0 ways to make change.

We can easily translate this description into a recursive procedure:

<pre>
(define (count-change amount)  
  (cc amount 5))  
(define (cc amount kinds-of-coins)  
  (cond ((= amount 0) 1)  
        ((or (< amount 0) (= kinds-of-coins 0)) 0)  
        (else (+ (cc amount  
                     (- kinds-of-coins 1))  
                 (cc (- amount  
                        (first-denomination kinds-of-coins))  
                     kinds-of-coins)))))  
(define (first-denomination kinds-of-coins)  
  (cond ((= kinds-of-coins 1) 1)  
        ((= kinds-of-coins 2) 5)  
        ((= kinds-of-coins 3) 10)  
        ((= kinds-of-coins 4) 25)  
        ((= kinds-of-coins 5) 50)))  
</pre>

(The `first-denomination` procedure takes as input the number of kinds of coins available and returns the denomination of the first kind. Here we are thinking of the coins as arranged in order from largest to smallest, but any order would do as well.) We can now answer our original question about changing a dollar:

<pre>
(count-change 100)  
<em>292</em>  
</pre>

`Count-change` generates a tree-recursive process with redundancies similar to those in our first implementation of `fib`. (It will take quite a while for that 292 to be computed.) On the other hand, it is not obvious how to design a better algorithm for computing the result, and we leave this problem as a challenge. The observation that a tree-recursive process may be highly inefficient but often easy to specify and understand has led people to propose that one could get the best of both worlds by designing a "smart compiler" that could transform tree-recursive procedures into more efficient procedures that compute the same result.[^34]

<strong id="exercise1_11">Exercise 1.11.</strong>  A function _f_ is defined by the rule that _f_(_n_) = _n_ if _n_<3 and _f_(_n_) = _f_(_n_ - 1) + 2_f_(_n_ - 2) + 3_f_(_n_ - 3) if _n_≥ 3\. Write a procedure that computes _f_ by means of a recursive process. Write a procedure that computes _f_ by means of an iterative process.

<strong id="exercise1_12">Exercise 1.12.</strong>  The following pattern of numbers is called _Pascal's triangle_.

![](images/12.gif)

The numbers at the edge of the triangle are all 1, and each number inside the triangle is the sum of the two numbers above it.[^35] Write a procedure that computes elements of Pascal's triangle by means of a recursive process.

<strong id="exercise1_13">Exercise 1.13.</strong>  Prove that _Fib_(_n_) is the closest integer to φ<sup>_n_</sup> /√5, where φ = (1 + √5)/2\. Hint: Let ψ = (1 - √5)/2\. Use induction and the definition of the Fibonacci numbers (see section [1.2.2](1-2-2-tree-recursion.md)) to prove that _Fib_(_n_) = (φ<sup>_n_</sup> - ψ<sup>_n_</sup>)/√5.

---

[^32]: An example of this was hinted at in section [1.1.3](1-1-3-evaluating-combinations.md): The interpreter itself evaluates expressions using a tree-recursive process.

[^33]: For example, work through in detail how the reduction rule applies to the problem of making change for 10 cents using pennies and nickels.

[^34]: One approach to coping with redundant computations is to arrange matters so that we automatically construct a table of values as they are computed. Each time we are asked to apply the procedure to some argument, we first look to see if the value is already stored in the table, in which case we avoid performing the redundant computation. This strategy, known as _tabulation_ or _memoization_, can be implemented in a straightforward way. Tabulation can sometimes be used to transform processes that require an exponential number of steps (such as `count-change`) into processes whose space and time requirements grow linearly with the input. See exercise [3.27](#exercise3_27).

[^35]: The elements of Pascal's triangle are called the _binomial coefficients_, because the <em>n</em>th row consists of the coefficients of the terms in the expansion of (_x_ + _y_)<sup>_n_</sup>. This pattern for computing the coefficients appeared in Blaise Pascal's 1653 seminal work on probability theory, _Traité du triangle arithmétique_. According to Knuth (1973), the same pattern appears in the _Szu-yuen Yü-chien_ ("The Precious Mirror of the Four Elements"), published by the Chinese mathematician Chu Shih-chieh in 1303, in the works of the twelfth-century Persian poet and mathematician Omar Khayyam, and in the works of the twelfth-century Hindu mathematician Bháscara Áchárya.
