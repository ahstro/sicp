# Structure and Interpretation of Computer Programs - GitBook version

## [Read it here](http://ahstro.gitlab.io/sicp)

#### Description
This is a [GitBook](https://www.gitbook.com/) version of Hal Abelson's, Jerry Sussman's and Julie Sussman's [_Structure and Interpretation of Computer Programs_](https://mitpress.mit.edu/sicp/). The text was taken from the official page on the 30th of August, 2016, and has not been modified except for the few points described in [Changes](ALTERATIONS.md).

#### Licensing
Reproduced under the [Creative Commons Attribution-ShareAlike 4.0 license](http://creativecommons.org/licenses/by-sa/4.0/), as published by [the MIT Press](https://mitpress.mit.edu/).
