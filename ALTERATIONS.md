This project aims to be as true to the original as possible, however, if any changes have had to be made, they're listed here:

- Double back ticks (`) and apostrophes (') have been replaced by double quotes ("), because the former messed up the GitBook styling
- Long, single page sections (e.g., 1.1) that contain a bunch of subsections have been split up into separate files. This lends itself better to the GitBook paging system and I'm not even sure doing it with ids and page jumping is possible.
- Use unicode symbols instead of gif images from the site, where applicable. This allows the symbols to use the correct color and makes them easier to use in the markdown. (Some examples of symbols used: ↑,  φ, and √)
